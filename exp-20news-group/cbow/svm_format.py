def svm_format(dirname,svm_file,file_flag,flag):

	f_svm = open(svm_file,'w')
	for i in xrange(1,21):
		filename = file_flag+'-'+flag+'-'+str(i)+'-vectors.txt'
		with open(dirname+'/'+filename,'r') as f:
			for line in f:
				new_line = line.split(' ')[1:-1]
				svm_format_1line = [str(index+1)+':'+item for index,item in enumerate(new_line)]
				f_svm.write(str(i)+' '+' '.join(svm_format_1line)+'\n')

if __name__ == "__main__":
	#generate training file
	svm_format('../../20-news-train-vectors-100','cbow-train','cbow','train')
	#generate testing file
	svm_format('../../20-news-test-vectors-100','cbow-test','cbow','test')

