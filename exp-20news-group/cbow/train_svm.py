import os
def svm(training_file,testing_file):
	training_parameter = './train -s 2 -c 1 -B -1 '+training_file+' training.model'
	os.system(training_parameter)
	testing_parameter = './predict '+testing_file+' training.model output'
	os.system(testing_parameter)

if __name__ == "__main__":
	training_file = 'cbow-train'
	testing_file = 'cbow-test'
	svm(training_file,testing_file)
