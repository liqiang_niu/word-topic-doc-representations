# README #

# **Jointly learning distributed representations of words, topics and documents.** #
## Li-Qiang Niu, 2015.1.22, NJU ##

# * **Introduction** #
## 1. **files** ##
### ------data: datasets and preprocessing tools ###
### ------TW-LW: TW, LW, TW-LW models ###
### ------DTW: DTW model ###
### ------exp-word-analogy: word analogies experiment ### 
### ------exp-word-similarity: word similarity experiment ###
### ------exp-topic-words: topic words, TW vs. LDA ###
### ------exp-20newsgroup: multi-class text classification ###
### ------ ###
### ------ ###
## 2. **contribution** ##

# * **Our models** #
## 1. **Our model architectures** ##
### ------ ###
![model.jpg](https://bitbucket.org/repo/M6kgXk/images/3726580399-model.jpg)

## 2.**TW** ## 
### ------ ###
## 3. **LW** ##
### ------ ###
## 4. **TW-LW** ##
### ------ ###
## 5. **DTW** ##
### ------ ###
# * **Experiments** #
## 1. **word analogies** ##
### ------ ###
## 2. **word similarity** ##
### ------ ###
## 3. **topic words** ##
### ------ ###
## 4. **text classification** ##
### ------ ###
# * **Conclusions** #
## ------ ##